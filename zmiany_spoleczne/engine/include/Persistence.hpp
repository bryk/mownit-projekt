/*
 * Persistence.hpp
 *
 *  Created on: 30-05-2012
 *      Author: nidys
 */

#ifndef PERSISTENCE_HPP_
#define PERSISTENCE_HPP_

#include <vector>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <utility>

using namespace std;

class Persistence {
public:
	Persistence();
	~Persistence();
	bool resize(int);
	int getN();
	void showGird();
	int getOccuranceSize() {
		return 1000;
	}
	bool tryPersuasion(int x, int y);
	vector<int> ile_takich_jest;
	vector<int> ile_razy_zmienili;
	vector<int> ile_bylo_prob_zmiany;
private:
	vector<vector<int> > mesh;
	//map<double, vector<double> >::iterator it;
	//map<double, vector<double> > occurance; // [sila_odpornosci] [ile_takich_jest ; ile_razy_zmienili ; Ile_bylo_prob_zmiany]

	bool loaded;
	int findPosition(int i, int j, int opt);
};

#endif /* PERSISTENCE_HPP_ */
