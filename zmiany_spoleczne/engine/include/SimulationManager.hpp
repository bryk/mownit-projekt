/*
 * SimulationManager.hpp
 *
 *  Created on: 25-04-2012
 *      Author: nidys
 */

#ifndef SIMULATIONMANAGER_HPP_
#define SIMULATIONMANAGER_HPP_

#include <string>
#include <cstddef>
#include <map>
#include <utility>
#include <cstdlib>
#include <cctype>
#include <ctime>
#include <fstream>
#include <climits>
#include <cmath>
#include <sstream>
#include <algorithm>
#include <iterator>
#include "engine/include/Grid.hpp"
#include "engine/include/SimulationStatistics.hpp"
#include "engine/include/Persistence.hpp"
#include "engine/include/ViewInterceptor.hpp"
#include <boost/shared_ptr.hpp>

namespace Engine {

using namespace std;

/**
 * Definicja tablicy tablic, potem w kilku miejscach uzywana
 */
/**
 * Do prowadzenia statystyk
 */
typedef unsigned long int ulong;

class SimulationManager {
public:
	/**
	 * Konstruktor i Deskryptor, ich dzialanie jest obowiazkowe
	 */
	SimulationManager();
	~SimulationManager();
	/**
	 * Wczytuje konfiguracje z configFile, ktory ma schemat:
	 * grid_length  x //x - dlugosc boku siatki
	 * views_number y //y - ilosc pogladow,
	 * 					zalozenie: 0 < y < 0.3*x*x
	 * media        z //z - ilosc mediow, nastepne 'z' lini wylada tak:
	 * Ai Bi 		  // Ai - numer pogladu,
	 * 					 Bi - sila przekonania
	 *  			zalozenie:0 <= Ai < y
	 *  					  0.0 <= Bi <= 1.0 to prawdopodobienstwo sukcesu
	 *  Kilka mediow moze zachecac do tego samego pogladu,
	 *  z rozna lub taka sama moca (np. gazeta,telewizja,radio,...)
	 * @param configFile plik konfiguracyjny
	 * @param random ==true na wejsciu beda randomowe wartosci
	 * @return "" na sukces, komunikat bledu w postaci stringa
	 */
	string loadConfig(string configFile);
	/**
	 * Wykonuje krok/-i symulacji
	 * @param many moze wykonac kilka
	 * @return true na sukces, false jesli many<0 lub
	 * jesli nie byla wykonana metoda loadConfig
	 */
	bool nextStep(int many = 1);
	/**
	 * Zwraca aktualny stan grida
	 * @return vector<vector<int>> czyli siatke opisujaca
	 * poglady lub vektor 1x1 wypelniony 0 jesli nie wykonana
	 * byla metoda loadConfig
	 */
	boost::shared_ptr<Grid> getState();
	int getM();
	bool saveSummary();
private:
	bool inceptFromMedia(int amountOfPeople);
	void inceptFromNeighbors(int amountOfPeople);
	bool openFile(string config);
	fstream configurations;
	/**
	 * Siatka kazde pole reprezentuje czlowieka o int=id pogladu
	 */
	boost::shared_ptr<Grid> grid;
	/**
	 * Konfiguracja ilosci i mocy pogladow pobrana z pliku
	 * int-id pogladu
	 * double-prawdopodobienstwo incepcji pogladu
	 */
	vector<pair<int, double> > mediaViews;
	/**
	 * Do debugowania wyswietla grida
	 */
	void showGird();
	/**
	 * Trzyma informacje czy wykonano loadConfig
	 * i czy zakonczylo sie ono sukcesem
	 */
	bool loaded;

	int viewsNumber;
	/**
	 * Komunikaty bledow
	 */
	string wrongSchema;
	string wrongData;
	/**
	 * licznik pogladow w promieniu 5
	 */
	//vector<int> rviews;
	vector<int> maximal_views;
	/**
	 * Klasa pprzeprowadza statystyki
	 */
	SimulationStatistics stats;

	boost::shared_ptr<ViewInterceptor> interceptor;
};

} /* namespace Engine */
#endif /* SIMULATIONMANAGER_HPP_ */
