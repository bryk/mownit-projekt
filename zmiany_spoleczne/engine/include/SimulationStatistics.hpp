/*
 * SimulationStatistics.hpp
 *
 *  Created on: May 1, 2012
 *      Author: nidys
 */

#ifndef SIMULATIONSTATISTICS_HPP_
#define SIMULATIONSTATISTICS_HPP_

#include <vector>
#include "engine/include/Grid.hpp"
#include <boost/shared_ptr.hpp>

using namespace std;

class SimulationStatistics {
public:
	SimulationStatistics();
	~SimulationStatistics();
	void load(bool *loaded, boost::shared_ptr<Grid> grid, int *viewsNumber, vector<pair<int, double> > *mediaViews,
			long double familyAvg);
	bool createSummary();
	void statsPanic(int i);
private:
	boost::shared_ptr<Grid> grid;
	bool *loaded;
	int *viewsNumber;
	vector<pair<int, double> > *mediaViews;
	vector<int> location;
	int simulation_no;
	long double familyAvg;
};

#endif /* SIMULATIONSTATISTICS_HPP_ */
