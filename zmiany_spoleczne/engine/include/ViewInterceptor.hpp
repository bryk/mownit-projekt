/*
 * ViewInterceptor.hpp
 *
 *  Created on: Jun 13, 2012
 *      Author: piotr
 */

#ifndef VIEWINTERCEPTOR_HPP_
#define VIEWINTERCEPTOR_HPP_

#include <boost/shared_ptr.hpp>
#include <vector>

class Grid;

namespace Engine {
typedef std::vector<int> vect;

class ViewInterceptor {
public:
	ViewInterceptor(boost::shared_ptr<Grid> grid, int max_family_size);
	virtual ~ViewInterceptor();
	int getMaximalInterceptedView(int x, int y);
	long double familyAvgSize;

private:
	boost::shared_ptr<Grid> grid;
	vect *whoToIntercept;
	int max_family_size;
};

} /* namespace Engine */
#endif /* VIEWINTERCEPTOR_HPP_ */
