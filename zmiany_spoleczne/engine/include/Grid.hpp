/*
 * Grid.hpp
 *
 *  Created on: Apr 30, 2012
 *      Author: nidys
 */

#ifndef GRID_HPP_
#define GRID_HPP_

#include <vector>
#include <cstdio>
#include "engine/include/Persistence.hpp"
#include <boost/shared_ptr.hpp>

using namespace std;

class Grid {
public:
	Grid();
	~Grid();
	bool resize(int);
	/**
	 * @return -1 jesli nie zainicjalizowano jeszcze
	 */
	int getN();
	/**
	 * Numer pogladu na pozycji
	 * @return -1 jesli (x,y) sa bledne
	 */
	int getPosition(int x, int y);
	/**
	 * @return 01 jesli (x,y) sa bledne
	 */
	bool setPosition(int x, int y, int val);
	bool changePosition(int x, int y, int val);
	void showGird();
	/*
	 * mesh wyglada tak: mesh[0][0] = -1
	 */
	void clear();

	boost::shared_ptr<Persistence> getPersistence() {
		return persistence;
	}
private:
	vector<vector<int> > mesh;
	boost::shared_ptr<Persistence> persistence;
	bool loaded;
};

#endif /* GRID_HPP_ */
