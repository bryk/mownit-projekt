/*
 * Grid.cpp
 *
 *  Created on: Apr 30, 2012
 *      Author: nidys
 */

#include "engine/include/Grid.hpp"

Grid::Grid() :
		persistence(new Persistence()) {
	loaded = false;
}

Grid::~Grid() {
}
bool Grid::resize(int size) {
	if (size > 0) {
		if (!persistence->resize(size)) {
			fprintf(stderr, "Błąd: resize persistence w grid->resize");
			return false;
		}
		mesh.resize(size);
		for (int i = 0; i < (int) mesh.size(); ++i)
			mesh[i].resize(size, 0);
	} else {
		return false;
	}
	loaded = true;
	return true;
}
int Grid::getN() {
	if (loaded)
		return mesh.size();
	else
		return -1;
}
int Grid::getPosition(int x, int y) {
	if (x >= 0 && x < (int) mesh.size() && y >= 0 && y < (int) mesh[x].size())
		return mesh[x][y];
	else
		return -1;
}
bool Grid::setPosition(int x, int y, int val) {
	if (x >= 0 && x < (int) mesh.size() && y >= 0 && y < (int) mesh[x].size()) {
		mesh[x][y] = val;
//		}
		return true;
	} else
		return false;

}
bool Grid::changePosition(int x, int y, int val) {
	if (x >= 0 && x < (int) mesh.size() && y >= 0 && y < (int) mesh[x].size()) {
		if (this->persistence->tryPersuasion(x, y)) {
			mesh[x][y] = val;
		}
		return true;
	} else
		return false;

}
void Grid::showGird() {
	for (int i = 0; i < (int) mesh.size(); ++i) {
		printf("%d_", i);
		for (int j = 0; j < (int) mesh[i].size(); ++j)
			printf("%d, ", mesh[i][j]);
		printf("\n");
	}
	printf("--------------------\n");
	printf("A teraz chwilowo Persistence wypisze\n");
	this->persistence->showGird();
	for (int i = 0; i < this->persistence->getOccuranceSize(); i++) {
		printf("i=%d -- [%d][%d , %d , %d]\n", i, i, persistence->ile_takich_jest[i], persistence->ile_razy_zmienili[i],
				persistence->ile_bylo_prob_zmiany[i]);
	}
}

void Grid::clear() {
	for (int i = 0; i < (int) mesh.size(); ++i)
		mesh[i].clear();
	mesh.clear();

	mesh.resize(1);
	mesh[0].resize(1, -1);
}

