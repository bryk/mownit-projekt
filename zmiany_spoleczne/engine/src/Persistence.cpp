/*
 * Persistence.cpp
 *
 *  Created on: 30-05-2012
 *      Author: nidys
 */

#include "engine/include/Persistence.hpp"

Persistence::Persistence() {
	loaded = false;
}

Persistence::~Persistence() {
}

int Persistence::findPosition(int i, int j, int opt) {
	if (opt == 0) {
		if ((i - 1) > 0) {
			return (i - 1);
		} else if (j > 0) {
			return i;
		} else {
			return 0;
		}
	} else {
		if ((j - 1) > 0) {
			return (j - 1);
		} else {
			return j;
		}
	}
}
bool Persistence::resize(int size) {
	if (size > 0) {
		mesh.resize(size);
		for (int i = 0; i < (int) mesh.size(); ++i) {
			mesh[i].resize(size);
			for (int j = 0; j < (int) mesh[i].size(); j++) {
				if (rand() % 2 == 0) {
					mesh[i][j] = rand() % getOccuranceSize();
				} else {
					mesh[i][j] = mesh[findPosition(i, j, 0)][findPosition(i, j, 1)];
				}
			}
		}

		ile_takich_jest.resize(getOccuranceSize(), 0);
		ile_bylo_prob_zmiany.resize(getOccuranceSize(), 0);
		ile_razy_zmienili.resize(getOccuranceSize(), 0);
//		for (int i = 0; i < (int) mesh.size(); i++) {
//			for (int j = 0; j < (int) mesh.size(); j++) {
//				vector<double> v;
//				v.resize(3);
//				v[0] = 0.0;
//				v[1] = 0.0;
//				v[2] = 0.0;
//				occurance[mesh[i][j]] = v;
//			}
//		}
		for (int i = 0; i < (int) mesh.size(); i++) {
			for (int j = 0; j < (int) mesh.size(); j++) {
				ile_takich_jest[mesh[i][j]] += 1;
			}
		}
//		it = occurance.begin();
		loaded = true;
	} else {
		return false;
	}
	return true;
}
int Persistence::getN() {
	if (loaded) {
		return mesh.size();
	} else {
		return -1;
	}
}
void Persistence::showGird() {
	for (int i = 0; i < (int) mesh.size(); ++i) {
		printf("%d_", i);
		for (int j = 0; j < (int) mesh[i].size(); ++j)
			printf("%d, ", mesh[i][j]);
		printf("\n");
	}
	printf("--------------------\n");
}
bool Persistence::tryPersuasion(int x, int y) {
	ile_bylo_prob_zmiany[mesh[x][y]] += 1; // podjeto probe przekonania
	if ((rand() % (getOccuranceSize() + 1)) > mesh[x][y]) {
		ile_razy_zmienili[mesh[x][y]] += 1; //udalo sie przekonac grupe o takiej stanowczosci
		return true;
	} else {
		return false;
	}
}

