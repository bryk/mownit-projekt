/*
 * ViewInterceptor.cpp
 *
 *  Created on: Jun 13, 2012
 *      Author: piotr
 */

#include "engine/include/ViewInterceptor.hpp"
#include "engine/include/Grid.hpp"
#include <vector>
#include <cstdlib>
#include <map>

using namespace std;

namespace {
int positionToInt(int x, int y, int n) {
	return x * n + y;
}

int getX(int pos, int n) {
	return pos / n;
}

int getY(int pos, int n) {
	return pos % n;
}

int getFamilySize(int max) {
	return rand() % (max + 1);
}
}

#define N (grid->getN())

namespace Engine {

int ViewInterceptor::getMaximalInterceptedView(int x, int y) {
	map<int, int> rv;

	vect & toIntercept = whoToIntercept[positionToInt(x, y, N)];
	int a, b;
	for (size_t i = 0; i < toIntercept.size(); i++) {
		a = getX(toIntercept[i], N);
		b = getY(toIntercept[i], N);
		//printf("Sąsiad (%d, %d) to: (%d, %d)\n", x, y, a, b);
		rv[grid->getPosition(a, b)]++;
	}
	int max = -1;
	map<int, int>::iterator it;
	vect maximal_views;
	for (it = rv.begin(); it != rv.end(); it++) {
		if (it->second > max) {
			max = it->second;
			maximal_views.clear();
			maximal_views.push_back(it->first);
		} else if (it->second == max) {
			maximal_views.push_back(it->first);
		}
	}
	return maximal_views[rand() % maximal_views.size()];
}

ViewInterceptor::ViewInterceptor(boost::shared_ptr<Grid> g,	int max) :
		familyAvgSize(0.0), grid(g), max_family_size(max) {
//	srand(time(NULL));
	whoToIntercept = new vect[N * N];
	for (int x = 0; x < N; x++) {
		for (int y = 0; y < N; y++) {
			//insert neigbours
			for (int i = -2; i <= 2; ++i) {
				for (int j = -2; j <= 2; ++j) {
					if (i == 0 && j == 0) {
						continue;
					} else {
						int tmp = grid->getPosition(x + i, y + j);
						if (tmp >= 0) {
							whoToIntercept[positionToInt(x, y, N)].push_back(positionToInt(x + i, y + j, N));
						}
					}
				}
			}
			//insert family
			int familySz = getFamilySize(this->max_family_size);
			familyAvgSize += (long double) familySz;
			int a, b;
			for (int i = 0; i < familySz; i++) {
				do {
					a = rand() % N;
					b = rand() % N;
				} while (a == x && b == x);
				whoToIntercept[positionToInt(x, y, N)].push_back(positionToInt(a, b, N));
			}
		}
	}
	familyAvgSize /= (long double) N*N;
}

ViewInterceptor::~ViewInterceptor() {
	delete[] whoToIntercept;
}

} /* namespace Engine */
