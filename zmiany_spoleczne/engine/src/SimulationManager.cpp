/*
 * SimulationManager.cpp
 *
 *  Created on: 25-04-2012
 *      Author: nidys
 */

#include "engine/include/SimulationManager.hpp"
#include <map>

namespace Engine {

SimulationManager::SimulationManager() :
		grid(new Grid()) {
	wrongSchema = "Bład: Nie poprawny schemat pliku konfiguracyjnego";
	wrongData = "Bład: Dane nie spełniaja założen";

	// info o zaladowaniu konfiguracji
	loaded = false;
	viewsNumber = -1;
	srand(97);
}

SimulationManager::~SimulationManager() {
	fprintf(stderr, "usuwam simulation managera\n");
}
bool SimulationManager::openFile(string config) {
	configurations.open(config.c_str(), fstream::in);

	if (!(configurations.is_open() || configurations.good())) {
		loaded = false;
		return false;
	} else {
		loaded = true;
		return true;
	}
}
string SimulationManager::loadConfig(string configFile) {

	if (!openFile(configFile)) {
		return "Błąd: Nie otwarłem pliku: " + configFile;
	}

	// --- WCZYTYWANIE ---
	string tmp;
	configurations >> tmp; // wczytanie wartosci N do siatki NxN
	if (!configurations.good() || tmp != "grid_length") {
		loaded = false;
		return wrongSchema + " - nazwa 'grid_length'";
	}
	int tmpNumb;
	configurations >> tmpNumb;
	if (!configurations.good()) {
		loaded = false;
		return wrongSchema + " - wartość N";
	} else if (!grid->resize(tmpNumb)) {
		loaded = false;
		return wrongSchema + " - wartość N";
	}

	configurations >> tmp; // wczytanie wartosci M - ilosc pogladow
	if (!configurations.good() || tmp != "views_number") {
		loaded = false;
		return wrongSchema + " - nazwa 'views_number'";
	}
	configurations >> tmpNumb;
	if (!configurations.good()) {
		loaded = false;
		return wrongSchema + " - wartość M";
	} else {
		viewsNumber = tmpNumb;
		if (viewsNumber <= 0 || viewsNumber >= (0.3 * pow(grid->getN(), 2))) {
			loaded = false;
			return wrongData + " - ~(M << NxN)";
		} else {
			//rviews.resize(viewsNumber, 0);
			//maximal_views.resize(viewsNumber, 0);
		}
	}

	int max_family_size;
	configurations >> tmp; // maksymalna wielkość rodziny
	if (!configurations.good() || tmp != "max_family_size") {
		loaded = false;
		return wrongSchema + " - nazwa 'max_family_size'";
	}
	configurations >> tmpNumb;
	if (!configurations.good()) {
		loaded = false;
		return wrongSchema + " - wartość dla max_family_size";
	} else {
		max_family_size = tmpNumb;
		if (viewsNumber < 0 /* || viewsNumber >= 15*/) {
			loaded = false;
			return wrongData + " - maksymalna wielkość rodziny";
		}
	}

	configurations >> tmp; // wczytanie ilosci mediow
	if (!configurations.good() || tmp != "media") {
		loaded = false;
		return wrongSchema + " - nazwa 'media'";
	}
	string stmpNumb;
	configurations >> stmpNumb;
	tmpNumb = atoi(stmpNumb.c_str());
	if (!configurations.good() && !isdigit(stmpNumb[0])) {
		loaded = false;
		return wrongSchema + " - wartość określająca ilość mediów";
	}

	int view;
	double power;

	// wczytywanie mocy mediow
	if (tmpNumb != 0) {
		for (int i = 0; i < tmpNumb; ++i) {
			configurations >> view >> power;
			/* Jesli ma wczytac ostatnia linie to potrzebowalby delimiter='\n'
			 * ktorego dla wygody mozna nie wsadzac
			 * dlatego sprawdzam czy to byl koniec pliku .eof i czy wczytal
			 * wszystkie wartosci i == tmpNumb-1
			 */
			if (!configurations.good() && !(configurations.eof() || (i == tmpNumb - 1))) {
				loaded = false;
				return wrongSchema + " - ilość zadeklarowanych mediów się nie zgadza";
			} else {
				if (view >= viewsNumber || view < 0 || power < 0.0 || power > 1.0) {
					loaded = false;
					ostringstream errorInfo;
					if (view >= viewsNumber) {
						errorInfo << " - błędny numer poglądu = " << view;
						errorInfo << ", dla prawdopodobienstwa = " << power;
					} else {
						errorInfo << " - niemożliwe prawdopodobienstwo=" << power;
						errorInfo << ", dla poglądu = " << view;
					}
					return wrongData + errorInfo.str();
				} else {
					pair<int, double> viewTmp(view, power);
					mediaViews.insert(mediaViews.end(), viewTmp);
				}
			}
		}
	} else {
		pair<int, double> viewTmp(-1, 1.0);
		mediaViews.insert(mediaViews.end(), viewTmp);
	}
	for (int i = 0; i < grid->getN(); ++i)
		for (int j = 0; j < grid->getN(); ++j)
			grid->setPosition(i, j, rand() % viewsNumber);
	configFile.clear();

	interceptor.reset(new ViewInterceptor(grid, max_family_size));
	stats.load(&loaded, grid, &viewsNumber, &mediaViews, interceptor->familyAvgSize);
	configurations.close();
	return configFile;
}
bool SimulationManager::inceptFromMedia(int amountOfPeople) {
	pair<int, double> someView = mediaViews[rand() % mediaViews.size()]; //losowanie z jednego pogladu
	if ((rand() % 100) < (int) (100.0 * someView.second)) { //losowanie na podstawie mocy zajscia incepcji pogladu
		if (!(grid->changePosition(rand() % amountOfPeople, rand() % amountOfPeople, someView.first))) { // losowanie dla ktorego czlowieka zmieni sie poglad
			if (someView.first >= viewsNumber) {
				fprintf(stderr, "Błąd. Popraw mnie :(");
			}
			fprintf(stderr, "Błąd w setPosition");
			return false;
		}
	}
	return true;
}
//inceptFromNeighbors
void SimulationManager::inceptFromNeighbors(int amountOfPeople) {
	int x = rand() % amountOfPeople;
	int y = rand() % amountOfPeople;
	grid->changePosition(x, y, interceptor->getMaximalInterceptedView(x, y));
}

bool SimulationManager::nextStep(int many) {
	if (loaded == true) {
		if (many > 0 && many < INT_MAX) {
			int amountOfPeople = grid->getN();

			for (int i = 0; i < many; ++i) { //wykonaj many krokow
				int changeView = rand() % (int) pow(amountOfPeople, 2); // ile ludzi zmieni w tym kroku poglad

				for (int j = 0; j < changeView; ++j) {
					if (rand() % 2 == 0 && (mediaViews[0].first != -1)) {
						if (!inceptFromMedia(amountOfPeople)) {
							return false;
						}
					} else {
						inceptFromNeighbors(amountOfPeople);
					}
				}
			}
//			grid->showGird(); //debug
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}
boost::shared_ptr<Grid> SimulationManager::getState() {
	if (loaded == true) {
		return grid;
	} else {
		grid->clear();
		return grid;
	}
}
int SimulationManager::getM() {
	if (loaded) {
		return viewsNumber;
	} else {
		return -1;
	}
}
// --- STATYSTYKI ---
bool SimulationManager::saveSummary() {
	return stats.createSummary();
}
} /* namespace Engine */
