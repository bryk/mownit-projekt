/*
 * SimulationStatistics.cpp
 *
 *  Created on: May 1, 2012
 *      Author: nidys
 */

#include "engine/include/SimulationStatistics.hpp"
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <ctime>

SimulationStatistics::SimulationStatistics() {
	simulation_no = 0;
	loaded = NULL;
	viewsNumber = NULL;
	mediaViews = NULL;
}

void SimulationStatistics::load(bool *l, boost::shared_ptr<Grid> g, int *v, vector<pair<int, double> > *m,
		long double familyAvg) {
	if ((loaded = l) == NULL)
		statsPanic(1);
	grid = boost::shared_ptr<Grid>(g);
	this->familyAvg = familyAvg;
	if ((viewsNumber = v) == NULL)
		statsPanic(3); //M
	if ((mediaViews = m) == NULL)
		statsPanic(4); // size()=1, first=-1 PAMIETAJ!!

	if (*loaded) {
		location.clear();
		location.resize(*viewsNumber, 0);
		for (int i = 0; i < (*g).getN(); ++i)
			for (int j = 0; j < (*g).getN(); ++j)
				++location[(*g).getPosition(i, j)];

	}
}

SimulationStatistics::~SimulationStatistics() {
}

bool SimulationStatistics::createSummary() { //dodaj mozliwosc zapisywania wielokrotnie
	if (*loaded) {

		time_t rawtime;
		time(&rawtime);
		struct tm *czas = localtime(&rawtime);

		ostringstream outputFileName;
		outputFileName << "Summary/summary_";

		outputFileName << czas->tm_year + 1900 << "_";
		outputFileName << czas->tm_mon + 1 << "_";
		outputFileName << czas->tm_mday << "_";

		outputFileName << czas->tm_hour << ":";
		outputFileName << czas->tm_min << ":";
		outputFileName << czas->tm_sec;

		fstream results(outputFileName.str().c_str(), fstream::out);
		if (!(results.is_open() || results.good())) {
			fprintf(stderr, "Nie otwarłem pliku\n");
			return false;
		}

		results.setf(ios::fixed, ios::floatfield);

		results << "\n--- PODSUMOWANIE " << asctime(localtime(&rawtime)) << "\n";

		results << "Średnia ilość rodzin:\t" << this->familyAvg << endl << endl;

		results << "Sila_stanowczosci | Ilosc_osob | ilu_dalo_sieprzekonac | ile_prob_przekonania_podjeto\n";
		boost::shared_ptr<Persistence> persistence = grid->getPersistence();
		for (int i = 0; i < persistence->getOccuranceSize(); i++) {
			results << setw(17) << ((float) i / persistence->getOccuranceSize()) << " | " << setw(10)
					<< persistence->ile_takich_jest[i] << " | " << setw(21) << persistence->ile_razy_zmienili[i] << " | "
					<< setw(28) << persistence->ile_bylo_prob_zmiany[i] << endl;
		}
		results << endl << endl;

		results << "Poglad | Ilosc_poczatkowa | Ilosc_koncowa | Prawdopodobienstwo/-a | \n";

		vector<vector<double> > prob;
		prob.clear();
		prob.resize(*viewsNumber);
		for (int i = 0; i < (int) (*mediaViews).size(); ++i) {
			prob[(*mediaViews)[i].first].push_back((*mediaViews)[i].second);
		}

		vector<int> end_location;
		end_location.clear();
		end_location.resize(*viewsNumber, 0);
		for (int i = 0; i < (*grid).getN(); ++i)
			for (int j = 0; j < (*grid).getN(); ++j)
				++end_location[(*grid).getPosition(i, j)];

		for (int i = 0; i < (int) location.size(); ++i) {
			results << setw(6) << i << " | " << setw(16) << location[i] << " | " << setw(13) << end_location[i];
			if ((int) prob[i].size() == 0)
				results << " | \n";
			else {
				results << " | ";
				for (int j = 0; j < (int) prob[i].size(); ++j) {
					results << prob[i][j] << " ; ";
				}
				results << endl;
			}
		}

		results.close();
		++simulation_no;
		return true;
	} else {
		return false;
	}
}
void SimulationStatistics::statsPanic(int where) {
	fprintf(stderr, "SimulationStatistic get NULL pointed element at %d\n", where);
	exit(1);
}

