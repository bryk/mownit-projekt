TEMPLATE = app
TARGET = ZmianySpoleczne
QT += core \
    gui \
    svg \
    opengl

# dodalem zeby widzial naglowki silnika
# INCLUDEPATH = ./Engine
# dodalem zeby widzial pliki zrodlowe silnika
HEADERS += engine/include/ViewInterceptor.hpp \
    engine/include/Grid.hpp \
    engine/include/Persistence.hpp \
    engine/include/SimulationManager.hpp \
    engine/include/SimulationStatistics.hpp \
    gui/include/SimulationThread.hpp \
    gui/include/SimulationVisualizer.hpp \
    gui/include/VisualizationPainter.hpp \
    gui/include/MainWindow.hpp
SOURCES += engine/src/ViewInterceptor.cpp \
    engine/src/Grid.cpp \
    engine/src/Persistence.cpp \
    engine/src/SimulationManager.cpp \
    engine/src/SimulationStatistics.cpp \
    gui/impl/SimulationThread.cpp \
    gui/impl/SimulationVisualizer.cpp \
    gui/impl/VisualizationPainter.cpp \
    gui/impl/MainWindow.cpp \
    ZmianySpoleczne.cpp
FORMS += gui/MainWindow.ui
RESOURCES += 
