/*
 * VisualizationPainter.hpp
 *
 *  Created on: Apr 30, 2012
 *      Author: piotr
 */

#ifndef VISUALIZATIONPAINTER_HPP_
#define VISUALIZATIONPAINTER_HPP_

#include <QPen>
#include <QFont>
#include <QPainter>
#include <QPaintEvent>
#include <vector>
#include "engine/include/SimulationManager.hpp"
#include <boost/shared_ptr.hpp>
#include "engine/include/Grid.hpp"

class VisualizationPainter {
public:
	VisualizationPainter();
	virtual ~VisualizationPainter();
	void paint(QPainter *painter, const QRect &rect);
	void setManager(boost::shared_ptr<Engine::SimulationManager> mgr);

private:
	void clear(QPainter *painter, const QRect &rect);
	void fillElement(QPainter *painter, int x, int y, QColor &color);
	QColor * getColor(int num);

	boost::shared_ptr<Engine::SimulationManager> manager;
	vector<QColor> colors;
	boost::shared_ptr<QPixmap> old;
	QBrush background;
};

#endif /* VISUALIZATIONPAINTER_HPP_ */
