/*
 * SimulationThread.hpp
 *
 *  Created on: Apr 30, 2012
 *      Author: piotr
 */

#ifndef SIMULATIONTHREAD_HPP_
#define SIMULATIONTHREAD_HPP_

#include <QThread>
#include "engine/include/SimulationManager.hpp"
#include <boost/shared_ptr.hpp>

class SimulationThread: public QThread {
Q_OBJECT

public:
	SimulationThread(boost::shared_ptr<Engine::SimulationManager> manager);
	virtual ~SimulationThread();
	void run();
	void finish();

signals:
	void updateSimulationState();

private:
	QAtomicInt shouldFinish;
	boost::shared_ptr<Engine::SimulationManager> manager;
};

#endif /* SIMULATIONTHREAD_HPP_ */
