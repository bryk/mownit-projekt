#ifndef SIMULATIONVISUALIZATOR_H
#define SIMULATIONVISUALIZATOR_H

#include <QtGui/QWidget>
#include "gui/include/VisualizationPainter.hpp"
#include <QGLWidget>
#include <boost/shared_ptr.hpp>

class SimulationVisualizer: public QWidget {
Q_OBJECT

public:
	SimulationVisualizer(QWidget *parent = 0);
	~SimulationVisualizer();
	void setManager(boost::shared_ptr<Engine::SimulationManager> mgr);

public slots:
	void animate();

protected:
	void paintEvent(QPaintEvent *event);

private:
	VisualizationPainter *visualizationPainter;
};

#endif // SIMULATIONVISUALIZATOR_H
