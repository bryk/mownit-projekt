#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include "ui_MainWindow.h"
#include "gui/include/SimulationThread.hpp"
#include "engine/include/SimulationManager.hpp"

namespace Gui {
class MainWindow: public QMainWindow {
Q_OBJECT

public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();

public slots:
	void selectConfiguration();
	void initSimulation();
	void destroySimulation();
	void startContiniousSimulation();
	void stopContiniousSimulation();
	void nextStep();
	void saveSimulation();

signals:
	void configurationFileChanged(QString filename);
	void simulationStateChanged();

private:
	Ui::MainWindow ui;
	boost::shared_ptr<SimulationThread> simulationThread;
	boost::shared_ptr<Engine::SimulationManager> manager;
	void setSimulationControlButtonsEnabled(bool enabled);
};
}
#endif // MAINWINDOW_H
