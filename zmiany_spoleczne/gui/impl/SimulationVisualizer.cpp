#include "gui/include/SimulationVisualizer.hpp"
#include "gui/include/VisualizationPainter.hpp"
#include <QTimer>
#include <QPainter>
#include <QSizePolicy>
#include <cstdio>
#include <QElapsedTimer>
#include <boost/shared_ptr.hpp>

SimulationVisualizer::SimulationVisualizer(QWidget *parent) :
		QWidget(parent) {
	QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
	sizePolicy.setHorizontalStretch(0);
	sizePolicy.setVerticalStretch(0);
	sizePolicy.setHeightForWidth(this->sizePolicy().hasHeightForWidth());
	setSizePolicy(sizePolicy);
	setMinimumSize(300, 300);
	visualizationPainter = new VisualizationPainter();
}

void SimulationVisualizer::animate() {
	update();
}

void SimulationVisualizer::setManager(boost::shared_ptr<Engine::SimulationManager> mgr) {
	visualizationPainter->setManager(mgr);
	update();
}

void SimulationVisualizer::paintEvent(QPaintEvent *event) {
	//fprintf(stderr, "painEvent\n");
	QPainter painter(this);
	visualizationPainter->paint(&painter, event->rect());
}

SimulationVisualizer::~SimulationVisualizer() {

}
