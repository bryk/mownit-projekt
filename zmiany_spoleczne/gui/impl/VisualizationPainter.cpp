/*
 * VisualizationPainter.cpp
 *
 *  Created on: Apr 30, 2012
 *      Author: piotr
 */

#include "gui/include/VisualizationPainter.hpp"
#include "engine/include/SimulationManager.hpp"
#include <cstdlib>
#include <cstdio>
//#include <ctime>

VisualizationPainter::VisualizationPainter() :
		manager(), colors(0) {
	QLinearGradient gradient(QPointF(50, -20), QPointF(80, 20));
	gradient.setColorAt(0.0, Qt::white);
	gradient.setColorAt(1.0, QColor(0xa6, 0xce, 0x39));

	background = QBrush(Qt::white);
}

void VisualizationPainter::fillElement(QPainter *painter, int x, int y, QColor &color) {
	painter->setPen(color);
	painter->drawPoint(x, y);
}

void VisualizationPainter::clear(QPainter *painter, const QRect &rect) {
	painter->fillRect(rect, background);
}

void VisualizationPainter::setManager(boost::shared_ptr<Engine::SimulationManager> mgr) {
	colors.clear();
//	srand(time(NULL));
	//int k;
	if (mgr) {
		qreal step = (255.0 / (mgr->getM() - 1)) * 3.0f;
		int a = 0, b = 0, c = 0;
		for (int i = 0; i < mgr->getM(); i++) {
			//k = i + 1;
			if (i >= 0 && i < mgr->getM() / 3) {
				a++;
				b = 0;
				c = 0;
			} else if (i >= mgr->getM() / 3 && i < (mgr->getM() / 3) * 2) {
				a--;
				b++;
				c = 0;
			} else {
				a = 0;
				b--;
				c++;
				if (b < 0)
					b = 0;
			}

			colors.push_back(
					QColor((a * step > 255 ? 255 : a * step), (b * step > 255 ? 255 : b * step),
							(c * step > 255 ? 255 : c * step)));

//			fprintf(stderr, "a:%d, b:%d, c:%d\n", a, b, c);
//			colors.push_back(QColor((k % 5) * step, (k % 6) * step, (k % 7) * step));
		}
	}
	manager = mgr;
	old.reset();
}

QColor * VisualizationPainter::getColor(int num) {
	return &(colors[num]);
}

void VisualizationPainter::paint(QPainter *painter, const QRect &rect) {
	if (manager) {
		boost::shared_ptr<Grid> grid = manager->getState();
		int n = grid->getN();
		int height = rect.height();
		int width = rect.width();
		painter->scale(width / (qreal) n, height / (qreal) n);
		if (!old) {
			old.reset(new QPixmap(n, n));
		}
		QPainter copyPainter(old.get());

		for (int x = 0; x < n; x++) {
			for (int y = 0; y < n; y++) {
				int num = grid->getPosition(y, x);
				if (true) {
					//fprintf(stdout, "Przepisuje X:%d, Y:%d z %d na %d\n", x, y, previousState.getPosition(y, x), num);
					fillElement(&copyPainter, x, y, (*getColor(num)));
					//fillElement(painter, x, y, QColor(rand() % 255, rand() % 255, rand() % 255), n);
					//previousState.setPosition(y, x, num);
					//painted++;
				}
			}
		}
		copyPainter.end();
		painter->drawPixmap(0, 0, *old);
		painter->end();
	} else {
		clear(painter, rect);
	}
}

VisualizationPainter::~VisualizationPainter() {
}

