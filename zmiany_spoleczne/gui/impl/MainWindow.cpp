#include "gui/include/MainWindow.hpp"
#include "gui/include/SimulationThread.hpp"

#include <cstdio>
#include <QFileDialog>
#include <boost/shared_ptr.hpp>
#include <QMessageBox>

namespace Gui {
MainWindow::MainWindow(QWidget *parent) :
		QMainWindow(parent), manager() {
	ui.setupUi(this);
	ui.configurationPathInput->setText(tr("./basicConfig.conf"));
	ui.initSimulationButton->setEnabled(true);
}

MainWindow::~MainWindow() {

}

void MainWindow::selectConfiguration() {
	fprintf(stderr, "selectConfiguration\n");
	QString filename = QFileDialog::getOpenFileName(this, "Wybierz plik konfiguracji", QDir::currentPath(),
			tr("Pliki konfiguracji (*.conf)"));
	if (!filename.isNull()) {
		emit configurationFileChanged(filename);
		ui.initSimulationButton->setEnabled(true);
	}
}

void MainWindow::initSimulation() {
	fprintf(stderr, "initSimulation\n");
	manager.reset(new Engine::SimulationManager());
	string state = manager->loadConfig(ui.configurationPathInput->text().toStdString());
	if (state != "") {
		QMessageBox msgBox;
		msgBox.setText(trUtf8(state.c_str()));
		msgBox.exec();
	} else {
		connect(this, SIGNAL(simulationStateChanged()), ui.simulationVisualizer, SLOT(animate()));
		manager->nextStep(1);
		ui.simulationVisualizer->setManager(manager);
		ui.selectConfiguration->setEnabled(false);
		ui.initSimulationButton->setEnabled(false);
		ui.configurationPathInput->setEnabled(false);
		ui.destroySimulationButton->setEnabled(true);
		this->setSimulationControlButtonsEnabled(true);
	}
}

void MainWindow::destroySimulation() {
	fprintf(stderr, "destroySimulation\n");
	ui.destroySimulationButton->setEnabled(false);
	this->stopContiniousSimulation();
	manager.reset();
	ui.simulationVisualizer->setManager(manager);
	ui.selectConfiguration->setEnabled(true);
	ui.initSimulationButton->setEnabled(true);
	ui.configurationPathInput->setEnabled(true);
	this->setSimulationControlButtonsEnabled(false);
}

void MainWindow::setSimulationControlButtonsEnabled(bool enabled) {
	ui.startContiniousSimulationButton->setEnabled(enabled);
	ui.stopContiniousSimulationButton->setEnabled(false);
	ui.nextStepButton->setEnabled(enabled);
	ui.saveSimulationButton->setEnabled(enabled);
}

void MainWindow::startContiniousSimulation() {
	ui.startContiniousSimulationButton->setEnabled(false);
	ui.stopContiniousSimulationButton->setEnabled(true);
	ui.nextStepButton->setEnabled(false);
	ui.saveSimulationButton->setEnabled(false);
	simulationThread.reset(new SimulationThread(manager));
	connect(simulationThread.get(), SIGNAL(updateSimulationState()), ui.simulationVisualizer, SLOT(animate()));
	simulationThread->start(QThread::NormalPriority);
}

void MainWindow::stopContiniousSimulation() {
	ui.stopContiniousSimulationButton->setEnabled(false);
	ui.stopContiniousSimulationButton->repaint();
	if (simulationThread && simulationThread->isRunning()) {
		//simulationThread->deleteLater();
		simulationThread->finish();
		simulationThread->wait();
		simulationThread.reset();
	}
	ui.startContiniousSimulationButton->setEnabled(true);
	ui.nextStepButton->setEnabled(true);
	ui.saveSimulationButton->setEnabled(true);
}

void MainWindow::nextStep() {
	fprintf(stderr, "nextStep\n");
	manager->nextStep(1);
	emit simulationStateChanged();
}

void MainWindow::saveSimulation() {
	fprintf(stderr, "saveSimulation\n");
	if (!(manager->saveSummary())) {
		QMessageBox msgBox;
		msgBox.setText(trUtf8("Nie udało się zapisać podsumowania symulacji"));
		msgBox.exec();
	}
}
}
