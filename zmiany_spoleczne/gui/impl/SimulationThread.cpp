/*
 * SimulationThread.cpp
 *
 *  Created on: Apr 30, 2012
 *      Author: piotr
 */

#include "gui/include/SimulationThread.hpp"
#include <cstdio>

void SimulationThread::run() {
	while (!shouldFinish) {
		manager->nextStep(10);
		emit updateSimulationState();
	}
}

void SimulationThread::finish() {
	shouldFinish = 1;
}

SimulationThread::SimulationThread(boost::shared_ptr<Engine::SimulationManager> mgr) {
	shouldFinish = 0;
	this->manager = mgr;
}

SimulationThread::~SimulationThread() {
	fprintf(stderr, "Usuwam wątek\n");
}

